FROM python:3.11-slim-bullseye

RUN mkdir /beaurapi

WORKDIR /beaurapi

COPY requirements.txt requirements.txt


RUN apt-get -q update \
	&& DEBIAN_FRONTEND=noninteractive apt-get -yq --no-install-recommends install python3-dev libpq5 libpq-dev gcc libc6-dev git \
    && apt-get purge -y --auto-remove libpq-dev gcc libc6-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN pip install -r requirements.txt
RUN pip install aiohttp


RUN pip install git+https://gitlab.com/beaur1s/beauris.git

WORKDIR /beaurapi
COPY beaurapi.py boot.sh ./
RUN chmod +x boot.sh

ENV FLASK_APP beaurapi.py
EXPOSE 80

ENTRYPOINT ["/beaurapi/boot.sh"]
