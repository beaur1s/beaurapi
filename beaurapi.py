import os

from beauris import Beauris

from flask import Flask, jsonify, request


default_beauris_root_dir = "data_beaurapi"

BEAURIS_ROOT_DIR = os.environ.get("BEAURIS_ROOT_DIR", default_beauris_root_dir)

default_organisms_path = "{}/organisms".format(BEAURIS_ROOT_DIR)
ORGANISMS_PATH = os.environ.get("ORGANISMS_PATH", default_organisms_path)

default_config_file = "{}/beauris.yml".format(BEAURIS_ROOT_DIR)
BEAURIS_CONFIG_FILE = os.environ.get("BEAURIS_CONFIG_FILE",
                                     default_config_file)


bo = Beauris(root_work_dir=BEAURIS_ROOT_DIR, config_file=BEAURIS_CONFIG_FILE)
list_files = bo.load_organisms(ORGANISMS_PATH)

app = Flask(__name__)


def files_to_dict(list_files):
    dict = {}
    for f in list_files:
        dict[f.computer_name] = f.get_metadata()

    return dict


def filter_dict_by_group(dict, group):

    group = group.split(',')
    response = {}
    for k, v in dict.items():
        if not v.get("restricted_to"):
            v["restricted_to"] = "public"
        if any(g in v["restricted_to"] for g in group):
            response[k] = v

    return response


dictionnary = files_to_dict(list_files)


@app.errorhandler(404)
def not_found(error):
    return jsonify({"error": "Not found"}), 404


@app.route("/organisms/")
def get_all_organisms():

    filtered_dict = filter_dict_by_group(dictionnary, request.headers.get
                                         ("Remote-Groups", ""))

    return jsonify(filtered_dict)


@app.route("/organisms/<string:organism>/")
def get_organism(organism):

    filtered_dict = filter_dict_by_group(dictionnary, request.headers.get
                                         ("Remote-Groups", ""))
    try:
        response = filtered_dict[organism]
        response = jsonify(response)
    except KeyError:
        response = "ERROR : Route does not exist."

    return response


@app.route("/organisms/attr/<string:attr>")
def get_metadata_organisms(attr):

    filtered_dict = filter_dict_by_group(dictionnary, request.headers.get
                                         ("Remote-Groups", ""))
    try:
        response = [sub_dict[attr] for sub_dict in filtered_dict.values()]
    except KeyError:
        response = "ERROR : Route does not exist."

    return response


@app.route("/organisms/tags/<string:attr>")
def get_organisms_by_tags(attr):
    if ("," in attr):
        attr = set(attr.split(","))
    else:
        attr = {attr}

    response = []
    try:
        response = files_to_dict(bo.load_organisms_by_tags(
            ORGANISMS_PATH, with_tags=attr))
        response = filter_dict_by_group(response, request.headers.get(
            "Remote-Groups", ""))
    except KeyError:
        response = "ERROR : Route does not exist."

    return response


@app.route("/organisms/paths/tags/<string:attr>")
def get_paths_organisms_by_tags(attr):
    if ("," in attr):
        attr = set(attr.split(","))
    else:
        attr = {attr}

    response = []
    try:
        paths = []

        organisms = bo.load_organisms_by_tags(
            ORGANISMS_PATH, with_tags=attr)

        for organism in organisms:
            childrens = organism.get_children()
            for child in childrens:
                files = child.input_files
                paths.append([file.path for file in files.values()])

        response = paths
    except KeyError:
        response = "ERROR : Route does not exist."

    return response


if __name__ == '__main__':
    app.run()
